## Docker images for continuous integration

**Images:**
- mingw_base: Base image for mingw-w64 builds
- dispatch_ng: For testing dispatch_ng (based on mingw_base)

All images are built using gitlab-ci infrastructure.
