#!/bin/bash

set -x
set -e

export project_registry="$1"
export img="$2"
option="$3"
export rd="/opt/image/$img"

if test "$option" = "gen-dockerfile"; then
	source "./recipe.sh"
	{
		echo "FROM $base_image"
		echo "ADD . ${rd}/"
		echo "RUN ${rd}/main.sh $project_registry $img"
		echo "$dockerfile_append"
	} > Dockerfile
else
	source "$rd/recipe.sh"

	#Setup the system for building
	if test -f "/opt/setup_done"; then :; else
		{
			echo "#Enable multilib (needed for wine)"
			echo "[multilib]"
			echo "Include = /etc/pacman.d/mirrorlist"
		} >> /etc/pacman.conf
		pacman -Syu --needed --noconfirm base-devel git sudo
		echo "build_user ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/build_user
		useradd --system build_user
		echo "MAKEFLAGS=\"-j`nproc`\"" >> /etc/makepkg.conf
		touch "/opt/setup_done"
	fi

	#Installs package from AUR
	aur_install()
	{
		package=$1

		echo "## Building AUR package $package"

		mkdir -p /opt/aur
		chmod -R 777 /opt/aur

		(
			cd /opt/aur
			sudo -u build_user git clone "https://aur.archlinux.org/$package.git"
			cd "$package"
			if test -f "$rd/$package.aur.patch"; then
				patch -p 1 -i "$rd/$package.aur.patch"
			fi
			#TODO: A general way to find required PGP keys is needed
			#      to remove the --skippgpcheck option.
			if yes | sudo -u build_user makepkg -sri --skippgpcheck &> /tmp/build.log; then :; else
				cat /tmp/build.log
				exit 1
			fi
		)

		rm -fR /opt/aur
	}

	run_recipe

	#Cleanup
	rm -fR /var/cache/pacman/pkg

	echo "Build complete"
fi
